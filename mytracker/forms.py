from django import forms
from django.contrib.auth.models import Group
from django.forms import ModelForm
from mytracker.models import Task, Project


class SearchForm(forms.Form):
    """Search."""

    q = forms.CharField(
        widget=forms.widgets.TextInput(attrs={'size': 35})
    )


class AddTaskListForm(ModelForm):
    """The picklist showing allowable groups to which a new list can be added
    determines which groups the user belongs to. This queries the form object
    to derive that list."""

    def __init__(self, user, *args, **kwargs):
        super(AddTaskListForm, self).__init__(*args, **kwargs)
        self.fields['group'].queryset = Group.objects.filter(user=user)
        self.fields['group'].widget.attrs = {
            'id': 'id_group', 'class': "custom-select mb-3", 'name': 'group'}

    class Meta:
        model = Project
        exclude = ['created_date', 'slug', ]