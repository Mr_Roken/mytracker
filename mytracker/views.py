from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.utils import timezone
from django.db import IntegrityError
from django.conf import settings
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import Group
from django.db.models import Q
from django.utils.text import slugify
import datetime
from tracker.task import Task
from tracker.userSession import Session

from mytracker.models import Task as Task_Model, Project, DjangoStorage, Comment

from mytracker.forms import SearchForm, AddTaskListForm


@login_required
def projects(request) -> HttpResponse:
    """Homepage view - projects a user can view, and ability to add a list.
    """

    thedate = datetime.datetime.now()
    searchform = SearchForm(auto_id=False)

    # Make sure user belongs to at least one group.
    if request.user.groups.all().count() == 0:
        messages.warning(request, "You do not yet belong to any groups. Ask your administrator to add you to one.")

    # Superusers see all lists
    if request.user.is_superuser:
        lists = Project.objects.all().order_by('group', 'name')
    else:
        lists = Project.objects.filter(group__in=request.user.groups.all()).order_by('group', 'name')

    list_count = lists.count()

    # superusers see all lists, so count shouldn't filter by just lists the admin belongs to
    if request.user.is_superuser:
        task_count = Task_Model.objects.filter(completed=0).count()
    else:
        task_count = Task_Model.objects.filter(completed=0).filter(task_list__group__in=request.user.groups.all()).count()

    context = {
       "lists": lists,
       "thedate": thedate,
       "searchform": searchform,
       "list_count": list_count,
       "task_count": task_count,
    }

    return render(request, 'resources/projects.html', context)


@login_required
def list_detail(request, list_id=None, list_slug=None, view_completed=False):
    """Display and manage tasks in a project.
    """

    # Defaults
    task_list = None
    form = None

    # Which tasks to show on this list view?
    if list_slug == "mine":
        tasks = Task_Model.objects.filter(host=request.user.username)

    else:
        # Show a specific list, ensuring permissions.
        task_list = get_object_or_404(Project, id=list_id)
        if task_list.group not in request.user.groups.all() and not request.user.is_staff:
            raise PermissionDenied
        tasks = Task_Model.objects.filter(task_list=task_list.id)

    # Additional filtering
    if view_completed:
        tasks = tasks.filter(completed=True)
    else:
        tasks = tasks.filter(completed=False)

    all_tasks = Task_Model.objects.order_by('-priority')
    manager = Session()
    manager.data = DjangoStorage()

    for task in all_tasks:
        cur_task = manager.data.get_task(task.id)
        if cur_task.period:
            planner(cur_task)

    if request.method == 'POST':
        if 'create' in request.POST:
            manager = Session()
            manager.data = DjangoStorage()
            model = Task_Model()
            model.save()
            new_task = Task(key=model.id)
            if request.POST.get('parent_task') != 'None':
                new_task.parent = request.POST.get('parent_task')
            new_task.name = request.POST.get('name_task')
            new_task.host = request.user.username
            new_task.type_task = request.POST.get('type_task')
            new_task.priority = request.POST.get('priority_task')
            new_task.description = request.POST.get('description')
            new_task.status = request.POST.get('status_task')
            new_task.period = request.POST.get('period')
            new_task.admins = [request.user.username]
            new_task.members = [request.user.username]
            new_task.project = task_list.id

            start_date = request.POST.get('date_start')
            start_time = request.POST.get('time_start')

            if not start_date:
                start_date = str(datetime.datetime.now().strftime('%Y-%m-%d'))
            if not start_time:
                start_time = str('0:00')
            new_task.start_time = start_date + ' ' + start_time

            end_date = request.POST.get('date_end')
            end_time = request.POST.get('time_end')

            if end_time or end_date:
                if not end_date:
                    end_date = str(datetime.datetime.now().strftime('%Y-%m-%d'))
                if not end_time:
                    end_time = str(datetime.datetime.now().strftime('%H:%M'))
                new_task.deadline = end_date + ' ' + end_time

            if new_task.start_time and new_task.deadline:
                if new_task.start_time > new_task.deadline:
                    model.delete()
                    return HttpResponseRedirect('/add_task')

            manager.add_task(new_task)

            if new_task.parent:
                return HttpResponseRedirect('/task/{}/'.format(new_task.parent))
        else:
            pass

    return render(request, 'resources/project_details.html', locals())


@login_required
def task_detail(request, task_id: int) -> HttpResponse:
    """View task details. Allow task details to be edited. Process new comments on task.
    """

    task = get_object_or_404(Task_Model, pk=task_id)
    comment_list = Comment.objects.filter(task=task_id)

    # Ensure user has permission to view task. Admins can view all tasks.
    # # Get the group this task belongs to, and check whether current user is a member of that group.
    # if task.task_list.group not in request.user.groups.all() and not request.user.is_staff:
    #     raise PermissionDenied

    name = request.user.username
    cur_date = datetime.datetime.now().strftime("%H:%M %d-%m-%Y")
    if request.method == 'POST':
        manager = Session()
        manager.data = DjangoStorage()
        if 'add_admin' in request.POST:
            name_new_admin = request.POST.get('new_admin')
            if name_new_admin != '':
                try:
                    manager.add_admin_in_task(task_id, name_new_admin)
                except:
                    pass
        elif 'remove_admin' in request.POST:
            name_removed_admin = request.POST.get('remove_admin')
            try:
                manager.delete_admin_in_task(task_id, name_removed_admin)
            except:
                pass
        elif 'add_member' in request.POST:
            name_new_member = request.POST.get('new_member')
            if name_new_member != '':
                try:
                    manager.add_member_in_task(task_id, name_new_member)
                except:
                    pass
        elif 'remove_member' in request.POST:
            name_remove_member = request.POST.get('remove_member')
            try:
                manager.delete_member_in_task(task_id, name_remove_member)
            except:
                pass
        elif 'add_link' in request.POST:
            key_second_task = request.POST.get('new_link')
            try:
                manager.add_link(task_id, key_second_task)
            except:
                pass
        elif 'remove_link' in request.POST:
            key_second_task = request.POST.get('remove_link')
            try:
                manager.delete_link(task_id, key_second_task)
            except:
                pass
        elif 'info_button' in request.POST:
            return redirect(reverse('task_detail', kwargs={'task_id': request.POST.get('info_button')}))
        elif 'add_comment' in request.POST:
            Comment.objects.create(
                author=request.user,
                task=task,
                body=request.POST['comment-body'],
            )
        elif 'create' in request.POST:
            manager = Session()
            manager.data = DjangoStorage()
            model = Task_Model()
            model.save()
            new_task = Task(key=model.id)
            if request.POST.get('parent_task') != 'None':
                new_task.parent = request.POST.get('parent_task')
            new_task.name = request.POST.get('name_task')
            new_task.host = request.user.username
            new_task.type_task = request.POST.get('type_task')
            new_task.priority = request.POST.get('priority_task')
            new_task.description = request.POST.get('description')
            new_task.status = request.POST.get('status_task')
            new_task.period = request.POST.get('period')
            new_task.admins = [request.user.username]
            new_task.members = [request.user.username]
            new_task.project = task.task_list.id

            start_date = request.POST.get('date_start')
            start_time = request.POST.get('time_start')

            if not start_date:
                start_date = str(datetime.datetime.now().strftime('%Y-%m-%d'))
            if not start_time:
                start_time = str('0:00')
            new_task.start_time = start_date + ' ' + start_time

            end_date = request.POST.get('date_end')
            end_time = request.POST.get('time_end')

            if end_time or end_date:
                if not end_date:
                    end_date = str(datetime.datetime.now().strftime('%Y-%m-%d'))
                if not end_time:
                    end_time = str(datetime.datetime.now().strftime('%H:%M'))
                new_task.deadline = end_date + ' ' + end_time

            if new_task.start_time and new_task.deadline:
                if new_task.start_time > new_task.deadline:
                    model.delete()
                    return redirect(reverse('task_detail', kwargs={'task_id': task_id}))

            manager.add_task(new_task)

            if new_task.parent:
                return redirect(reverse('task_detail', kwargs={'task_id': new_task.parent}))
        else:
            pass
    else:
        key_task = task_id

    task = Task_Model.objects.get(id=task_id)
    if not task.period:
        task.period = ''

    full_start_time = task.start_time + datetime.timedelta(hours=3)
    start_date = str(full_start_time).split()[0]
    start_time = str(full_start_time).split()[1][:5]

    if task.end_time:
        full_end_time = task.end_time + datetime.timedelta(hours=3)
        end_date = str(full_end_time).split()[0]
        end_time = str(full_end_time).split()[1][:5]
    else:
        end_date = None
        end_time = None

    admins = task.admins.split('|')
    if admins[0] == '':
        admins.pop(0)

    members = task.members.split('|')
    if members[0] == '':
        members.pop(0)

    key_links = task.links.split('|')
    if key_links[0] == '':
        key_links.pop(0)

    links = []
    for key in key_links:
        links.append(Task_Model.objects.get(id=key))

    key_subtasks = task.subtasks.split('|')
    if key_subtasks[0] == '':
        key_subtasks.pop(0)

    subtasks = []
    for key in key_subtasks:
        subtasks.append(Task_Model.objects.get(id=key))

    block_button = name not in admins

    if name not in admins and name not in members:
        return HttpResponseRedirect('/list_detail')

    return render(request, 'resources/info_task.html', locals())


def update_task(request, task_id):
    key_task = task_id
    task = Task_Model.objects.get(id=key_task)
    if 'ch_name' in request.POST:
        task.name = request.POST.get('ch_name')
    elif 'ch_status' in request.POST:
        status = request.POST.get('ch_status')
        if status == 'completed':
            task.completed = True
        else:
            task.completed = False
    elif 'ch_priority' in request.POST:
        task.priority = request.POST.get('ch_priority')
    elif 'ch_descr' in request.POST:
        task.description = request.POST.get('ch_descr')
    elif 'ch_period' in request.POST:
        task.period = request.POST.get('ch_period')
        task.time_last_copy = str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M'))
    else:
        start_date = request.POST.get('date_start')
        start_time = request.POST.get('time_start')

        print(start_time, start_date)

        if not start_date:
            start_date = str(task.start_time).split()[0]
        if not start_time:
            start_time = str(task.start_time).split()[1]
        task.start_time = start_date + ' ' + start_time

        end_date = request.POST.get('date_end')
        end_time = request.POST.get('time_end')

        if end_time or end_date:
            if not end_date:
                end_date = str(task.end_time).split()[0]
            if not end_date:
                end_date = str(datetime.datetime.now().strftime('%Y-%m-%d'))
            if not end_time:
                end_time = str(task.end_time).split()[0]
            if not end_time:
                end_time = str(datetime.datetime.now().strftime('%H:%M'))
            task.end_time = end_date + ' ' + end_time

    task.change_time = str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M'))
    task.save()
    return redirect(reverse('list_detail', kwargs={"list_id": task.task_list.id, "list_slug": task.task_list.slug}))


@login_required
def toggle_done(request, task_id: int) -> HttpResponse:
    """Toggle the completed status of a task from done to undone, or vice versa.
    Redirect to the list from which the task came.
    """

    task = get_object_or_404(Task_Model, pk=task_id)

    # Permissions
    if not (
        (task.host == request.user) or
        (task.task_list.group in request.user.groups.all())
    ):
        raise PermissionDenied

    tlist = task.task_list
    task.completed = not task.completed
    task.save()

    messages.success(request, "Task status changed for '{}'".format(task.name))
    return redirect(reverse('list_detail', kwargs={"list_id": tlist.id, "list_slug": tlist.slug}))


@login_required
def del_list(request, list_id: int, list_slug: str) -> HttpResponse:
    """Delete an entire list. Danger Will Robinson! Only staff members should be allowed to access this view.
    """
    task_list = get_object_or_404(Project, slug=list_slug)

    # Ensure user has permission to delete list. Admins can delete all lists.
    # Get the group this list belongs to, and check whether current user is a member of that group.
    if task_list.group not in request.user.groups.all() and not request.user.is_staff:
        raise PermissionDenied

    if request.method == 'POST':
        Project.objects.get(id=task_list.id).delete()
        messages.success(request, "{list_name} is gone.".format(list_name=task_list.name))
        return redirect('projects')
    else:
        task_count_done = Task_Model.objects.filter(task_list=task_list.id, completed=True).count()
        task_count_undone = Task_Model.objects.filter(task_list=task_list.id, completed=False).count()
        task_count_total = Task_Model.objects.filter(task_list=task_list.id).count()

    context = {
        "task_list": task_list,
        "task_count_done": task_count_done,
        "task_count_undone": task_count_undone,
        "task_count_total": task_count_total,
    }

    return render(request, 'resources/del_project.html', context)


@login_required
def search(request) -> HttpResponse:
    """Search for tasks user has permission to see.
    """
    if request.GET:

        query_string = ''
        found_tasks = None
        if ('q' in request.GET) and request.GET['q'].strip():
            query_string = request.GET['q']

            found_tasks = Task_Model.objects.filter(
                Q(name__icontains=query_string) |
                Q(description__icontains=query_string)
            )
        else:
            # What if they selected the "completed" toggle but didn't enter a query string?
            # We still need found_tasks in a queryset so it can be "excluded" below.
            found_tasks = Task_Model.objects.all()

        if 'inc_complete' in request.GET:
            found_tasks = found_tasks.exclude(completed=True)

    else:
        query_string = None
        found_tasks =None

    # Only include tasks that are in groups of which this user is a member:
    if not request.user.is_superuser:
        found_tasks = found_tasks.filter(task_list__group__in=request.user.groups.all())

    context = {
        'query_string': query_string,
        'found_tasks': found_tasks
    }
    return render(request, 'resources/search_results.html', context)


@login_required
def reorder_tasks(request) -> HttpResponse:
    """Handle task re-ordering (priorities) from JQuery drag/drop in project_detail.html
    """
    newtasklist = request.POST.getlist('tasktable[]')
    if newtasklist:
        # First task in received list is always empty - remove it
        del newtasklist[0]

        # Re-prioritize each task in list
        i = 1
        for id in newtasklist:
            task = Task_Model.objects.get(pk=id)
            task.priority = i
            task.save()
            i += 1

    # All views must return an httpresponse of some kind ... without this we get
    # error 500s in the log even though things look peachy in the browser.
    return HttpResponse(status=201)


def add_list(request) -> HttpResponse:
    """Allow users to add a new project.
    """

    if request.POST:
        form = AddTaskListForm(request.user, request.POST)
        if form.is_valid():
            try:
                newlist = form.save(commit=False)
                newlist.slug = slugify(newlist.name)
                newlist.save()
                messages.success(request, "A new list has been added.")
                return redirect('projects')

            except IntegrityError:
                messages.warning(
                    request,
                    "There was a problem saving the new list. "
                    "Most likely a list with the same name in the same group already exists.")
    else:
        if request.user.groups.all().count() == 1:
            form = AddTaskListForm(request.user, initial={"group": request.user.groups.all()[0]})
        else:
            form = AddTaskListForm(request.user)

    context = {
        "form": form,
    }

    return render(request, 'resources/add_project.html', context)


@login_required
def delete_task(request, task_id: int) -> HttpResponse:
    """Delete specified task.
    Redirect to the list from which the task came.
    """
    dtask = get_object_or_404(Task_Model, pk=task_id)
    tlist = dtask.task_list

    manager = Session()
    manager.data = DjangoStorage()
    removed_list = manager.get_keys_all_tree(task_id)
    try:
        manager.delete_task(task_id)
    except Exception as ex:
        print(ex)
    for task in removed_list:
        model_task = Task_Model.objects.get(id=task.key)
        model_task.delete()
        messages.success(request, "Task '{}' has been deleted".format(task.name))

    return redirect(reverse('list_detail', kwargs={"list_id": tlist.id, "list_slug": tlist.slug}))


def planner(task):
    cur_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
    manager = Session()
    manager.data = DjangoStorage()

    time_last_copy = (task.time_last_copy + datetime.timedelta(hours=3)).strftime('%Y-%m-%d %H:%M')

    while datetime.datetime.strptime(time_last_copy, "%Y-%m-%d %H:%M") < datetime.datetime.strptime(cur_time, "%Y-%m-%d %H:%M"):
        print('NEW', time_last_copy)
        model = Task_Model()
        model.save()
        new_task = Task(key=model.id)
        new_task.name = task.name
        new_task.host = task.host
        new_task.parent = task.parent
        new_task.type_task = task.type_task
        new_task.priority = task.priority
        new_task.status = task.status
        new_task.description = task.description
        new_task.start_time = task.start_time
        new_task.end_time = task.end_time
        new_task.admins = task.admins.copy()
        new_task.members = task.members.copy()
        manager.add_task(new_task)
        new_task.period = ''
        time_last_copy = date_translation(time_last_copy, task.period)

    print(task.time_last_copy, '+')
    print(time_last_copy)
    task.time_last_copy = time_last_copy
    manager.data.save_task(task)


def date_translation(date_str, period):
    time = datetime.datetime.strptime(date_str, "%Y-%m-%d %H:%M")
    periods = period.split("/")

    time = time.replace(year=time.year + int(periods[0]) // 12)

    try:
        time = time.replace(month=time.month + (int(periods[0]) % 12))
    except:
        # if count month > 12 after added new month
        time = time.replace(year=time.year + 1, month=time.month + (int(periods[0]) % 12) - 12)

    time = time + datetime.timedelta(days=int(periods[1]), hours=int(periods[2]), minutes=int(periods[3]))

    return time.strftime("%Y-%m-%d %H:%M")


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.groups.add(Group.objects.get(name=getattr(settings, "REGISTRATION_DEFAULT_GROUP_NAME", None)))
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})
