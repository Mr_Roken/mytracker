from django.db import models
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import Group
from django.utils import timezone
from django.conf import settings
from django.urls import reverse
from datetime import datetime
from tracker.task import Task as LibTask

# Create your models here.


class Project(models.Model):
    name = models.CharField(max_length=60)
    slug = models.SlugField(default='',)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, default='',)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = "Projects"

        # Prevents (at the database level) creation of two lists with the same slug in the same group
        unique_together = ("group", "slug")


class Task(models.Model):
    name = models.CharField(max_length=256, blank=True, null=True)
    task_list = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)
    completed = models.BooleanField(default=False)
    completed_date = models.DateField(blank=True, null=True)
    host = models.CharField(max_length=256, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    priority = models.PositiveIntegerField(default=3, null=True)

    parent = models.IntegerField(default=-1)
    create_time = models.DateTimeField(default='1998-12-08 0:00')
    change_time = models.DateTimeField(default='1998-12-08 0:00')
    time_last_copy = models.DateTimeField(default='1998-12-08 0:00')
    start_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    period = models.CharField(max_length=128, blank=True, null=True)

    subtasks = models.TextField(blank=True)
    links = models.TextField(blank=True)
    admins = models.TextField(blank=True)
    members = models.TextField(blank=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('task_detail', kwargs={'task_id': self.id, })

        # Auto-set the Task creation / completed date

    def save(self, **kwargs):
        # If Task is being marked complete, set the completed_date
        if self.completed:
            self.completed_date = datetime.now()
        super(Task, self).save()


class DjangoStorage:

    @staticmethod
    def save_task(task):
        new_task = Task.objects.get(id=task.key)
        if new_task:
            new_task.name = task.name
            new_task.host = task.host
            if task.parent:
                new_task.parent = task.parent
            new_task.priority = task.priority
            if task.creation_date:
                new_task.create_time = task.creation_date
            else:
                new_task.create_time = str(datetime.now().strftime("%Y-%m-%d %H:%M"))

            if task.changed_date:
                new_task.change_time = task.changed_date
            else:
                new_task.change_time = str(datetime.now().strftime("%Y-%m-%d %H:%M"))

            if task.admins:
                new_task.admins = '|'.join(task.admins)
            else:
                new_task.admins = ''

            if task.time_last_copy:
                new_task.time_last_copy = task.time_last_copy
            else:
                new_task.time_last_copy = str(datetime.now().strftime("%Y-%m-%d %H:%M"))

            if task.members:
                new_task.members = '|'.join(task.members)
            else:
                new_task.members = ''

            if task.links:
                new_task.links = '|'.join(task.links)
            else:
                new_task.links = ''

            str_sub = [str(x) for x in task.subtasks]

            print(str_sub)

            if str_sub:
                new_task.subtasks = '|'.join(str_sub)
            else:
                new_task.subtasks = ''

            new_task.description = task.description
            new_task.start_time = task.start_time
            new_task.end_time = task.deadline
            new_task.period = task.period
            new_task.task_list = get_object_or_404(Project, id=task.project)
            new_task.save()

    @staticmethod
    def get_task(key_task):
        try:
            task = Task.objects.get(id=key_task)
            lib_task = LibTask(key=key_task, creation_date=task.create_time, changed_date=task.change_time)
            lib_task.name = task.name
            lib_task.host = task.host
            if task.parent != -1:
                lib_task.parent = task.parent
            lib_task.priority = task.priority
            lib_task.status = task.completed
            lib_task.description = task.description
            lib_task.start_time = task.start_time
            lib_task.deadline = task.end_time
            lib_task.project = task.task_list.id
            if not task.period:
                lib_task.period = ''
            else:
                lib_task.period = task.period
            lib_task.time_last_copy = task.time_last_copy

            if task.admins != '':
                lib_task.admins = task.admins.split('|')

            if task.members != '':
                lib_task.members = task.members.split('|')

            if task.links != '':
                lib_task.links = task.links.split('|')

            if task.subtasks != '':
                lib_task.subtasks = task.subtasks.split('|')

            return lib_task
        except:
            return


class Comment(models.Model):
    """
    Not using Django's built-in comments because we want to be able to save
    a comment and change task details at the same time. Rolling our own since it's easy.
    """
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    date = models.DateTimeField(default=datetime.now)
    body = models.TextField(blank=True)

    def snippet(self):
        # Define here rather than in __str__ so we can use it in the admin list_display
        return "{author} - {snippet}...".format(author=self.author, snippet=self.body[:35])

    def __str__(self):
        return self.snippet