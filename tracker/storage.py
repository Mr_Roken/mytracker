import os
import json
from tracker.task import Task
import random
from collections import deque
from datetime import datetime, timedelta


class Storage:

    __DEFAULT_TASKS_FILE = os.path.dirname(__file__) + '/data/tasks.txt'

    def __init__(self, path_to_task_file=None):
        if path_to_task_file:
            self.path_to_task_file = path_to_task_file
        else:
            self.path_to_task_file = self.__DEFAULT_TASKS_FILE
            print(self.__DEFAULT_TASKS_FILE)
        print(path_to_task_file)

    def get_free_key_task(self):
        keys = []

        with open(self.path_to_task_file, 'r') as file:
            for line in file:
                current_task = Task()
                current_task.load(line)
                keys.append(current_task.key)

        while True:
            if len(keys) == 400:
                return None
            key = random.randint(0, 400)
            if key not in keys:
                return str(key)

    def save_task(self, task):
        if type(task) != Task:
            raise Exception("Object type is not Task")

        with open(self.path_to_task_file, 'a') as output:
            json.dump(task.__dict__, output)
            output.write('\n')

    def get_task(self, key_task):
        task = None
        scanned_tasks = []

        with open(self.path_to_task_file, 'r') as file:
            for line in file:
                current_task = Task()
                current_task.load(line)

                if current_task.key == key_task:
                    task = current_task
                else:
                    scanned_tasks.append(line)

        self.check_time(task)
        self.save_scanned_tasks(scanned_tasks)  # return unsuccessful tasks in file
        return task

    def save_scanned_tasks(self, scanned_tasks):
        with open(self.path_to_task_file, 'w') as output:
            for json_task in scanned_tasks:
                output.write(json_task)

    def get_all_tasks(self):
        tasks = []

        with open(self.path_to_task_file, 'r') as file:
            for line in file:
                task = Task()
                task.load(line)
                tasks.append(task)
        with open(self.path_to_task_file, 'w'):
            pass

        for task in tasks:
            self.check_time(task)

        return tasks

    def get_keys_tasks(self):
        keys_task = []
        scanned_task = []

        with open(self.path_to_task_file, 'r') as file:
            for line in file:
                task = Task()
                task.load(line)
                keys_task.append(task.key)
                scanned_task.append(line)

        self.save_scanned_tasks(scanned_task)  # return unsuccessful tasks in file
        return keys_task

    def get_keys_all_subtasks_tree(self, key_main_task=None):
        q = deque()

        task = self.get_task(key_main_task)
        if not task:
            return None

        q.append(task)
        list_tasks_project = [task]
        self.queue_on_project(q, list_tasks_project)
        self.save_task(task)
        return list_tasks_project

    def queue_on_project(self, q, list_tasks_project):
        if not len(q):
            return
        else:
            task = q.popleft()
            for key_sub in task.subtasks:
                sub = self.get_task(key_sub)
                list_tasks_project.append(sub)
                q.append(sub)
                self.save_task(sub)

            self.queue_on_project(q, list_tasks_project)

    def get_all_users(self):
        set_users = set()
        scanned_task = []

        with open(self.path_to_task_file, 'r') as file:
            for line in file:
                task = Task()
                task.load(line)
                for user in task.admins:
                    set_users.add(user)
                for user in task.members:
                    set_users.add(user)
                scanned_task.append(line)

        self.save_scanned_tasks(scanned_task)  # return unsuccessful tasks in file
        return set_users

    def get_user_task(self, name_user):
        user_tasks = []
        scanned_task = []

        with open(self.path_to_task_file, 'r') as file:
            for line in file:
                task = Task()
                task.load(line)
                if name_user in task.admins:
                    user_tasks.append(task)
                elif name_user in task.members:
                    user_tasks.append(task)
                else:
                    scanned_task.append(line)

        self.save_scanned_tasks(scanned_task)  # return unsuccessful tasks in file
        return user_tasks

    @classmethod
    def check_time(cls, task):
        if not task:
            return
        cur_time = datetime.now().strftime("%Y-%m-%d %H:%M")
        if task.deadline and task.deadline != "" and task.period and task.period != "":
            # if this task has end time and period and end time has passed, we  will move time this step period
            # while end time less then current time
            while datetime.strptime(cur_time, "%Y-%m-%d %H:%M") >= datetime.strptime(task.deadline, "%d/%m/%Y %H:%M"):
                task.start_time_for_plan = cls.date_translation(task.start_time_for_plan, task.period)
                task.deadline = cls.date_translation(task.deadline, task.period)

    @staticmethod
    def date_translation(date_str, period):
        time = datetime.strptime(date_str, "%Y-%m-%d %H:%M")
        periods = period.split("/")

        time = time.replace(year=time.year + int(periods[0]) // 12)

        try:
            time = time.replace(month=time.month + (int(periods[0]) % 12))
        except:
            # if count month > 12 after added new month
            time = time.replace(year=time.year + 1, month=time.month + (int(periods[0]) % 12) - 12)

        time = time + timedelta(days=int(periods[1]), hours=int(periods[2]), minutes=int(periods[3]))

        return time.strftime("%Y-%m-%d %H:%M")