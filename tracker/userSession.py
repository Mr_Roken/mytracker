from tracker.task import Task
import logging
from datetime import datetime
from tracker.storage import Storage
from tracker.logger import Logger
from collections import deque


class Session:
    def __init__(self, path_to_file=None):
        self.data = Storage(path_to_file)

    def add_task(self, task=None):
        """Adds task to select user/task and save this task in self.storage

        :param
        'task': saved object

        :return key saved task

        :raise
        'TypeError': if task not type Task
        'Exception': exceeded number of tasks or incorrect key
        """
        if type(task) != Task:
            raise TypeError("Object type is not Task")

        if task.key:
            if task.parent:
                parent_task = self.data.get_task(task.parent)
                if not parent_task:
                    self.error_key_task(task.parent)
                else:
                    parent_task.subtasks.append(task.key)
                    task.parent = parent_task.key
                    task.host = parent_task.host
                    task.admins = parent_task.admins.copy()
                    Logger.get_logger().debug("Task N{0} was added how subtask to task N{1}".format(task.key,
                                                                                                    parent_task.key))
                    self.data.save_task(parent_task)

            self.data.save_task(task)
            Logger.get_logger().debug("Task N{0} was added to self.data".format(task.key))
            return task.key
        else:
            Logger.get_logger().error("The free number for the task was not found")
            raise Exception("The task limit is exceeded")

    def save_task(self, task=None):
        """Check object on type Task and save task in self.storage

        :param
        'task': saved object

        :raise
        'TypeError': if task not type Task
        """
        if type(task) != Task:
            raise TypeError("Object type is not Task")
        self.data.save_task(task)

    @staticmethod
    def error_key_task(key_task):
        """Generate Exception in task with this key nor found in file

        :param
        'key_task': errors key task

        :return 'True' if the function succeeded

        :raise
        'Exception': task not found in file
        """
        Logger.get_logger().error("Task with number N{0} not found".format(key_task))
        raise Exception("Task N{0} not found in file".format(key_task))

    def add_admin_in_task(self, key_task, new_admin):
        """Adds admin to select task

        :param
        'key_task': key select task
        'new_admin': name new admin from task

        :return 'True' if the function succeeded

        :raise
        'Exception': admin already exist
        """
        task = self.data.get_task(key_task)

        if not task:
            self.error_key_task(key_task)

        if new_admin not in task.admins:
            task.admins.append(new_admin)
            Logger.get_logger().debug("Added admin {0} to task N{1}".format(new_admin, key_task))
            #self.data.save_message(new_admin, "User {0} was added how admin to task N{1}".format(new_admin, key_task))
            self.data.save_task(task)
            return True
        else:
            Logger.get_logger().error("Admin {0} already exist in task N{1}".format(new_admin, key_task))
            self.data.save_task(task)
            raise Exception("Admin {0} already exist in task N{1}".format(new_admin, key_task))

    def add_member_in_task(self, key_task, new_member):
        """Adds member to select task

        :param
        'key_task': key select task
        'new_member': name new member from task

        :return 'True' if the function succeeded

        :raise
        'Exception': member already exist
        """
        task = self.data.get_task(key_task)

        if not task:
            self.error_key_task(key_task)

        if new_member not in task.members:
            task.members.append(new_member)
            Logger.get_logger().debug("Added member {0} to tree N{1}".format(new_member, key_task))
            #self.data.save_message(new_member, "User {0} was added how member to task N{1}".format(new_member,
             #                                                                                         key_task))
            self.data.save_task(task)
            return True
        else:
            Logger.get_logger().error("Member {0} already exist in task N{1}".format(new_member, key_task))
            self.data.save_task(task)
            raise Exception("Member {0} already exist in task N{1}".format(new_member, key_task))

    def delete_admin_in_task(self, key_task, key_admin):
        """Removed admin to select task

        :param
        'key_task': key select task
        'key_admin': name removed admin from task

        :raise
        'Exception': admin not found in task
        """
        task = self.data.get_task(key_task)

        if not task:
            self.error_key_task(key_task)

        if key_admin in task.admins:
            task.admins.remove(key_admin)
            Logger.get_logger().debug("Removed admin {0} to task N{1}".format(key_admin, key_task))
            # self.data.save_message(key_admin, "User {0} was removed how admin to task N{1}".format(key_admin,
            #                                                                                           key_task))
            self.data.save_task(task)
            return True
        else:
            Logger.get_logger().error("Admin {0} not found in task N{1}".format(key_admin, key_task))
            self.data.save_task(task)
            raise Exception("Admin {0} not found in task N{1}".format(key_admin, key_task))

    def delete_member_in_task(self, key_task, key_member):
        """Removed member to select task

        :param
        'key_task': key select task
        'key_member': name removed member from task

        :return 'True' if the function succeeded

        :raise
        'Exception': member not found in task
        """
        task = self.data.get_task(key_task)

        if not task:
            self.error_key_task(key_task)

        if key_member in task.members:
            task.members.remove(key_member)
            Logger.get_logger().debug("Removed member {0} to task N{1}".format(key_member, key_task))
            # self.data.save_message(key_member, "User {0} was removed how member to task N{1}".format(key_member,
            #                                                                                             key_task))
            self.data.save_task(task)
            return True
        else:
            Logger.get_logger().error("Member {0} not found in task N{1}".format(key_member, key_task))
            self.data.save_task(task)
            raise Exception("Member {0} not found in task N{1}".format(key_member, key_task))

    def get_changed_task_params(self, key_task=None):
        """Return list changed params task

        :param
        'key_task': select task

        :return list changed params
        """
        task = self.data.get_task(key_task)

        if not task:
            self.error_key_task(key_task)

        self.data.save_task(task)

        return task.return_changed_task_params()

    def set_new_params_task(self, key_task, new_params=None):
        """Set new params task

        :param
        'key_task': key changed task
        'params': list new params
        """
        task = self.data.get_task(key_task)

        if not task:
            self.error_key_task(key_task)
        task.set_new_params_task(new_params)
        Logger.get_logger().debug("Task N{0} was changed".format(key_task))
        # for admin in task.admins:
        #     self.data.save_message(admin, "Task N{0} was changed".format(key_task))
        self.data.save_task(task)

    def change_status_task(self, key_task, new_status=None):
        """Change status select task

        :param
        'key_task': key select task
        'status': new status task
        """
        task = self.data.get_task(key_task)

        if not task:
            self.error_key_task(key_task)
        task.status = new_status
        Logger.get_logger().debug("Status task N{0} was changed on {1}".format(key_task, new_status))
        # for admin in task.admins:
        #     self.data.save_message(admin, "Status task N{0} was changed".format(key_task))
        self.data.save_task(task)

    def delete_task(self, key_task=None):
        """Removed select task

        :param
        'key_task': key select task
        """
        task = self.data.get_task(key_task)

        if not task:
            self.error_key_task(key_task)

        keys_subtask = task.subtasks.copy()  # for recursive removed subtasks

        self.data.save_task(task)
        for subtask in keys_subtask:  # removed subtasks this task (use DFS)
            self.delete_task(subtask)

        task = self.data.get_task(key_task)
        # for admin in task.admins:
        #     self.storage.save_message(admin, "Task N{0} was removed".format(key_task))

        if task.parent:  # check on root task tree and removed link
            parent_task = self.data.get_task(task.parent)
            if not parent_task:
                self.error_key_task(task.parent)
            else:
                parent_task.subtasks.remove(key_task)
                self.data.save_task(parent_task)

        for link in task.links:
            link_task = self.data.get_task(link)
            if link_task:
                link_task.links.remove(key_task)
                self.data.save_task(link_task)

        Logger.get_logger().debug("Task N{0} was removed".format(key_task))

    def get_keys_all_tree(self, key_main_task=None):
        """Return list with keys all subtasks this tree (use BFS)

            :param
            key_main_task: key first task

            :return
            list tasks tree
        """
        queue_for_bfs = deque()

        task = self.data.get_task(key_main_task)
        if not task:
            return None

        queue_for_bfs.append(task)
        list_tasks_project = [task]
        self.queue_on_project(queue_for_bfs, list_tasks_project)
        self.save_task(task)
        return list_tasks_project

    def queue_on_project(self, queue_for_bfs, list_tasks_project):
        """Recursive function for collection all tasks this project in list(use BFS)

        :param
        queue_for_bfs: queue with unvisited tasks
        list_tasks_tree: list with subtasks select task
        """
        if not len(queue_for_bfs):
            return
        else:
            task = queue_for_bfs.popleft()
            for key_sub in task.subtasks:
                sub = self.data.get_task(key_sub)
                list_tasks_project.append(sub)
                queue_for_bfs.append(sub)
                self.save_task(sub)

            self.queue_on_project(queue_for_bfs, list_tasks_project)

    def add_link(self, key_first, key_second):
        """Adds link between tasks

        :param
        'key_first': key first task
        'key_second': key second task

        :return 'True' if the function succeeded

        :raise
        'Exception': same task keys are selected
        """
        if key_first == key_second:
            raise Exception("The same task keys are selected")
        first_task = self.data.get_task(key_first)
        second_task = self.data.get_task(key_second)

        if not first_task:
            if second_task:
                self.data.save_task(second_task)
            self.error_key_task(key_first)

        if not second_task:
            if first_task:
                self.data.save_task(first_task)
            self.error_key_task(key_second)

        if key_second in first_task.links or key_first in second_task.links:
            Logger.get_logger().error("Link between tasks N{0} and N{1} already exist".format(key_first, key_second))
            self.data.save_task(first_task)
            self.data.save_task(second_task)
            raise Exception("Link between tasks N{0} and N{1} already exist".format(key_first, key_second))
        else:
            first_task.links.append(key_second)
            second_task.links.append(key_first)
            Logger.get_logger().debug("Link between tasks N{0} and N{1} was created".format(key_first, key_second))

            self.data.save_task(first_task)
            self.data.save_task(second_task)
            return True

    def delete_link(self, key_first, key_second):
        """Removed link between tasks

        :param
        'key_first': key first task
        'key_second': key second task

        :return 'True' if the function succeeded

        :raise
        'Exception': same task keys are selected
        """
        if key_first == key_second:
            raise Exception("The same task keys are selected")

        first_task = self.data.get_task(key_first)
        second_task = self.data.get_task(key_second)

        if not first_task:
            if second_task:
                self.data.save_task(second_task)
            self.error_key_task(key_first)

        if not second_task:
            if first_task:
                self.data.save_task(first_task)
            self.error_key_task(key_second)

        if key_second not in first_task.links or key_first not in second_task.links:
            Logger.get_logger().error("Link between tasks N{0} and N{1} already removed".format(key_first, key_second))
            self.data.save_task(first_task)
            self.data.save_task(second_task)
            raise Exception("Link between tasks N{0} and N{1} already removed".format(key_first, key_second))
        else:
            first_task.links.remove(key_second)
            second_task.links.remove(key_first)
            Logger.get_logger().debug("Link between tasks N{0} and N{1} was created".format(key_first, key_second))

            self.data.save_task(first_task)
            self.data.save_task(second_task)
            return True