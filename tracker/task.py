from datetime import datetime
import logging
import json


class Task:
    def __init__(self, parent=None, name=None, host=None, key=None, priority=None,
                 status=None, start_time=None, deadline=None,
                 period=None, project=None,
                 creation_date=None, changed_date=None, admins=None, members=None, description=None, time_last_copy=None):
        self.name = name
        self.parent = parent
        self.host = host
        self.__key = key
        self.subtasks =[]

        self.links = []
        self.priority = priority
        self.status = status
        self.start_time = start_time
        self.deadline = deadline
        self.period = period
        self.project = project

        if host is not None:
            self.admins =[host]
            self.members = [host]
        else:
            self.admins = []
            self.members =[]

        if admins:
            self.admins += admins
        if members:
            self.members += members

        if not creation_date:
            self.__creation_date = str(datetime.now().strftime('%Y-%m-%d %H:%M'))
        else:
            self.__creation_date = creation_date

        if description:
            self.description = description
        else:
            self.description = ''

        if not changed_date:
            self.__changed_date = str(datetime.now().strftime('%Y-%m-%d %H:%M'))
        else:
            self.__changed_date = changed_date

        if not time_last_copy:
            self.time_last_copy = str(datetime.now().strftime("%Y-%m-%d %H:%M"))
        else:
            self.time_last_copy = time_last_copy

    @staticmethod
    def str_to_date(date):
        return datetime.strptime(date, '%Y-%m-%d %H:%M')

    def __str__(self):
        if self.parent:
            return 'N' + str(self.key) + ' ' + self.name + ' ' + '(pr N' + self.parent + ')'
        else:
            return 'N' + str(self.key) + ' ' + self.name

    @property
    def creation_date(self):
        return self.__creation_date

    @property
    def key(self):
        return self.__key

    @property
    def changed_date(self):
        return self.__changed_date

    def load(self, string):
        self.__dict__ = json.loads(string)

    def return_changed_task_params(self):
        """Return list changed params task
        list params included: name, priority, period, start time task, end time task, status and project

        :return list changed params
        """
        return [self.name, self.priority, self.period, self.start_time, self.deadline, self.status, self.project]

    def change_task(self, params):
        self.name = params[0]
        self.priority = params[1]
        self.period = params[2]
        self.start_time = params[3]
        self.deadline = params[4]
        self.status = params[5]
        self.project = params[6]
        self.__changed_date = str(datetime.now().strftime("%Y-%m-%d %H:%M"))