import logging
import os


class Logger:
    """
    Is intended for recording messages about all actions with the program

    Public Methods:

    'add_handler': set new handler logger or use default
    'get_logger': return logger with name 'tracker'
    'add': write actions in program (logger records with level 'DEBUG')
    'error': write errors in program (logger records with level 'ERROR')
    'login': write login in program (logger records with level 'INFO')
    'get_logger_output_with_level': return logger records with different levels (DEBUG, ERROR, INFO)
    'get_all_logger_output_message': return logger records with all levels
    'get_name_last_login_user': return name last login user in system


    Constant:

    '__DEFAULT_PATH_TO_LOGGER_OUTPUT' = 'new_snake.log'
    """

    __DEFAULT_PATH_TO_LOGGER_OUTPUT = 'new_snake.log'

    def __init__(self, path_to_logger_output=None, handler=None):
        if path_to_logger_output:
            self.path_to_logger_output = path_to_logger_output
        else:
            self.path_to_logger_output = self.__DEFAULT_PATH_TO_LOGGER_OUTPUT

        self.add_handler(handler)

    def add_handler(self, new_handler=None):
        """Set library logger new fileHandler. If fileHandler == None, sets default settings

        Default settings:
        logger.setLevel = 'DEBUG'
        path logger output for FileHandler in configuration file
        setFormatter = 'time' - 'level logging' - 'message'

        :param
        'new_handler': object types logging.FileHandler created outside
        """
        logger = logging.getLogger('tracker')
        logger.setLevel(logging.DEBUG)

        if not new_handler:
            fh = logging.FileHandler(self.path_to_logger_output)
            fh.setLevel(logging.DEBUG)
            fh.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s - %(pathname)s - %(lineno)s'))
            logger.addHandler(fh)
        else:
            if type(new_handler) != logging.FileHandler:
                raise Exception("Param new_handler not type logging.FileHandler")
            logger.addHandler(new_handler)

    def on_logger(self):
        logger = self.get_logger()
        logger.disabled = False

    def off_logger(self):
        logger = self.get_logger()
        logger.disabled = True

    @classmethod
    def get_logger(cls):
        """Return library logger with name 'tracker'"""
        return logging.getLogger('tracker')