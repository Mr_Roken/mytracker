"""myapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views
from mytracker import views


extra_patterns = [
    path('', views.projects, name="projects"),
    path(
        '<int:list_id>/<str:list_slug>/',
        views.list_detail,
        name='list_detail'),
    path(
        'task/<int:task_id>/',
        views.task_detail,
        name='task_detail'),

    path(
        'toggle_done/<int:task_id>/',
        views.toggle_done,
        name='task_toggle_done'),
    path(
        '<int:list_id>/<str:list_slug>/completed/',
        views.list_detail,
        {'view_completed': True},
        name='list_detail_completed'),
    path(
        '<int:list_id>/<str:list_slug>/delete/',
        views.del_list,
        name="del_list"),

    path(
        'reorder_tasks/',
        views.reorder_tasks,
        name="reorder_tasks"),

    path(
        'delete/<int:task_id>/',
        views.delete_task,
        name='delete_task'),

    path(
        'mine/',
        views.list_detail,
        {'list_slug': 'mine'},
        name="mine"),
    path(
        'add_list/',
        views.add_list,
        name="add_list"),
    path(
        'update_task/<int:task_id>',
        views.update_task,
        name='update_task'),
    path(
        'search/',
        views.search,
        name="search"),
    path(
        'login/',
        views.signup,
        name="signup"),
]


urlpatterns = [
    path('', TemplateView.as_view(template_name='resources/home.html'), name='home'),
    path('login/', auth_views.login, name='login'),
    path('logout/', auth_views.logout, {'next_page': '/'}, name='logout'),
    path('todo/', include(extra_patterns)),
    path('admin/', admin.site.urls),
]

urlpatterns += staticfiles_urlpatterns()
